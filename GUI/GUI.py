from Tkinter import *

class ProgramGUI:

    def __init__(self):
        self.window = Tk()
        self.window.geometry("840x500")
        self.window.title("Sniffer")
        explanation = """Packets"""
        Label(self.window, justify=RIGHT, padx=10, text=explanation).pack(side="top")
        self.listBox = Listbox(self.window)

        self.updates = []
        self.wifi_info = []
        self.i = 0

    def add_pause_button(self, fun):
        Button(self.window, text="Stop", fg="red", command=fun).pack(side="top")
        self.window.update()

    def get_window(self):
        return self.window

    def __colour(self, should_colour, currentLastIndex):
        if should_colour:
            self.listBox.itemconfig(currentLastIndex, {'bg': 'red'})

    def __addItemInListBox(self, network_packet):
        self.updates.append(network_packet)
        self.i +=1

        # if  self.i % 30 == 0:
        #     size = self.listBox.size()
        #     self.listBox.delete(0, size)

        if self.i % 30 == 0 or self.i == 1:
            for current_packet in self.updates:

                should_colour = False
                if current_packet.tcp_header != None and current_packet.tcp_header.payload != None and current_packet.tcp_header.payload != '':
                    should_colour = True
                currentLastIndex = self.listBox.size()

                ieee_header = current_packet.ieee_header
                if ieee_header != None:
                    self.listBox.insert(currentLastIndex, ieee_header)

                    self.__colour(should_colour, currentLastIndex)

                    currentLastIndex = currentLastIndex + 1

                ip_header = current_packet.ip_header
                if ip_header != None:
                    self.listBox.insert(currentLastIndex, ip_header)

                    self.__colour(should_colour, currentLastIndex)

                    currentLastIndex = currentLastIndex + 1

                tcp_header = current_packet.tcp_header
                if tcp_header != None:
                    self.listBox.insert(currentLastIndex, tcp_header)

                    self.__colour(should_colour, currentLastIndex)

                    currentLastIndex = currentLastIndex + 1

                self.listBox.insert(currentLastIndex, '')
                self.listBox.pack(padx=10, pady=10, fill=BOTH, expand=True)

            size = self.listBox.size()
            self.listBox.yview(size)

            self.updates = []

    def add_wifi_info(self, wifi_info):
        self.wifi_info.append(wifi_info)
        self.i += 1

        # if  self.i % 30 == 0:
        #     size = self.listBox.size()
        #     self.listBox.delete(0, size)

        if self.i % 2 == 0 or self.i == 1:

            for wifi_info in self.wifi_info:

                currentLastIndex = self.listBox.size()

                self.listBox.insert(currentLastIndex, '')

                self.listBox.itemconfig(currentLastIndex, {'bg': 'orange'})
                if wifi_info.signal_strength > -65:
                    self.listBox.itemconfig(currentLastIndex, {'bg': 'green'})

                currentLastIndex = currentLastIndex + 1
                self.listBox.insert(currentLastIndex, wifi_info)
                currentLastIndex = currentLastIndex + 1

                self.listBox.insert(currentLastIndex, '')
                self.listBox.pack(padx=10, pady=10, fill=BOTH, expand=True)


            size = self.listBox.size()
            self.listBox.yview(size)

            self.wifi_info = []
            self.window.update()


    def add_packet(self, network_packet):
        self.__addItemInListBox(network_packet)
        self.window.update()

