# Wifi Analyzer and Sniffer

## Description
This is a project done while pursuing my master degree in Distributed Systems at UBB Cluj.  

## Requirements of the project:
* the wifi analyzer should be used in monitor mode to capture information
about surrounding wifi networks and to display the following data about them: 
    * SSID 
    * Access Point MAC Address
    * wifi manufacturer
    * signal strength
    * channel
    * frequency
    * (not implemented) security capabilities (e.g. type of encryption)
    
    
* the packet sniffer should capture wireless packages and display the following packet info:
    * IEEE 802.11 Header (source and destination MAC Address)
    * IP Header (source and destination IP address and other present fields)
    * TCP Header (source and destination port and other fields)
    * (if a HTTP packet = destination port is 80) ASCII payload
    
## About implementation

The project was developed using python2.7 and following packages:<br/>
    * tkinter - for UI<br/>
    * scapy - for package capturing
    
 
For capturing wifi network packets (beacon frames) I used a usb wireless adapter - [See the product here](https://www.amazon.com/Panda-300Mbps-Wireless-USB-Adapter/dp/B00EQT0YK2/ref=pd_lpo_147_img_0/130-7358364-3313346?_encoding=UTF8&pd_rd_i=B00EQT0YK2&pd_rd_r=886fb141-77b4-4ffc-8c0a-c1681f70c7a2&pd_rd_w=IM8tc&pd_rd_wg=ahXiD&pf_rd_p=7b36d496-f366-4631-94d3-61b87b52511b&pf_rd_r=KMDDATAAEVQ95JJ08AV0&psc=1&refRID=KMDDATAAEVQ95JJ08AV0) 
 
## How data was parsed

### A. Wifi Network Analyser

- because we used a wireless adapter, every packet came with a Radio Tap Header with extra information about the network; this information is added by the wifi analyser and it is not present in default Wireless Management Information

#### Wifi Packet Structure

* Radio Tap Header - 18 bytes - Interpreted under 802.11 radio information
* Beacon Frame - 24 bytes
* Rest of information - Wireless Management Info where
    * the first 12 bytes Fixed Parameters
    * rest of the bytes are Tagged Parameters
    
* Tagged Parameters are counted starting from 0

1. SSID - first parameter from Tagged Parameters
    * 1byte = 00 - because is the first tagged parameter
    * 2byte - tag length - mainly the length of SSID
    * SSID
    
2. Access Point MAC Address
    * in Beacon Frame starting from 11byte - 16byte
    
3. Manufacturer is determined by the Access Point MAC Address using this curated list: <br/>
https://gitlab.com/wireshark/wireshark/-/raw/master/manuf

4. Signal strength - Radio Tap Header - 15byte

5. Channel - Radio Tap Header - 16 byte

6. Frequency - Radio Tap Header - 11-12bytes


I parsed the packets by transforming bytes into strings.<br/>
I also implemented a channel change every one second, to capture SSIDs and signal from different networks that broadcast to different channels.


### B. Packet Analyser

#### Packet structure

* IEEE 802.11 Header - first 14bytes
* IP Header - next 20bytes (20bytes standard + optional data; there is a length field but for simplicity I chose a standard length of 20bytes as this was the most often length)
* TCP Header - next 32bytes

1. IEEE 802.11 - source MAC - first 6bytes
2. IEEE 802.11 - destination MAC - next 6bytes
3. Type - next 2 bytes

    * 0806 = ARP
    * 0800 = IPV4
    
4. IP Header - Length = the last 4 bits of 1byte
5. IP Header - Time to live - 9byte
6. IP Header - Protocol - 10byte

    * 06 = TCP
    * 11 = UDP
    * 01 = ICMP
      
6. IP Header - IP source - 13-16byte
7. IP Header - IP destination - 17-20byte

if Protocol = TCP

8. TCP Header - TCP Source Port - first 2bytes
9. TCP Header - TCP Destination Port - next 2bytes
10. TCP Header - Sequence Number - next 4 bytes
11. TCP Header - Acknowledgment number - next 4 bytes

if TCP destination Port = 80

then get all remaining bytes calculated using TCP Header Length and transform them in ASCII
 
 
## Resources

* how to rename the wireless interface in Linux - https://rundata.wordpress.com/2013/11/20/renaming-network-interfaces/
* how to set the wireless adapter in monitor mode - https://www.cellstream.com/reference-reading/tipsandtricks/410-3-ways-to-put-your-wi-fi-interface-in-monitor-mode-in-linux
* https://stackoverflow.com/questions/30871871/capturing-beacon-frames-in-802-11
* https://stackoverflow.com/questions/20290385/beacon-frame-captured-by-libpcap-is-something-strange
* a lot of Wireshark packet analysing

  


