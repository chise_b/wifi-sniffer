from scapy.all import *
from scapy.layers.dot11 import Dot11

from domain.NetworkInfo import NetworkInfo

from domain.IEEEHeader import IEEEHeader
from domain.IPHeader import IPHeader
from domain.TCPHeader import TCPHeader
from domain.NetworkPacket import NetworkPacket

class Parser:

    def __init__(self, gui):
        # for WIFI Analyzer
        self.networks = {}
        # for Sniffer
        self.packets = []
        self.gui = gui
        self.stop_sniffer = False

    # bits = 8 for signed
    # bits = 16 for unsigned
    def hex_to_dec(self, hexstr, bits):
        value = int(hexstr,16)
        if value & (1 << (bits-1)):
            value -= 1 << bits
        return value

    def process_packet(self, packet):
        """
        This function is executed whenever a packet is sniffed
        """

        bytes_array = bytes_hex(packet)

        #IEEE 802.11 0 - 14 bytes
        ieee_header = bytes_array[:28]

        #source - mac address - 0 - 6 bytes
        raw_source_mac_address = ieee_header[:12]
        source_mac_address = self.create_mac_addres(raw_source_mac_address)

        #destination - mac address - 6 - 12 bytes
        raw_destination_mac_address = ieee_header[12:24]
        destination_mac_address = self.create_mac_addres(raw_destination_mac_address)

        #type - 12 - 14
        #possible type = ARP | IPV4
        raw_type = ieee_header[24:28]

        type = 'UNKNOWN'
        if raw_type == '0800':
            type = 'IPV4'
        elif raw_type == '0806':
            type = 'ARP'

        obj_ieee_header = IEEEHeader(source_mac_address, destination_mac_address, type)
        obj_ip_header = None
        obj_tcp_header = None

        # we only get IP Header if type = IPV4
        if type == 'IPV4':
            # IP Header 14 - 34 bytes
            ip_header = bytes_array[28:68]

            #length of antet 2/2 of 14' byte
            raw_length = ip_header[1:2]
            ip_header_length = self.hex_to_dec(raw_length, 16)

            #time to live - 9 byte
            raw_time_to_live = ip_header[16:18]
            time_to_live = self.hex_to_dec(raw_time_to_live, 16)

            #protocol - 10 byte
            raw_protocol = ip_header[18:20]
            protocol = ''
            if raw_protocol == '06':
                protocol = 'TCP'
            elif raw_protocol == '11':
                protocol = 'UDP'
            elif raw_protocol == '01':
                protocol = 'ICMP'
            elif raw_protocol == '02':
                protocol = 'IGMP'

            #source - ip address - 13 - 16 bytes
            raw_source_ip = ip_header[24:32]
            source_ip_address = self.create_ip_address(raw_source_ip)

            #destination - ip address - 17 - 21
            raw_destination_ip = ip_header[32:40]
            destination_ip_address = self.create_ip_address(raw_destination_ip)

            obj_ip_header = IPHeader(source_ip_address, destination_ip_address, time_to_live, protocol)

            if protocol == 'TCP':
                #TCP header 34 - 66 bytes
                tcp_header = bytes_array[68:132]

                #source port 0 - 2 bytes
                raw_tcp_source_port = tcp_header[0:4]
                tcp_source_port = int(raw_tcp_source_port,16)

                #destination port 2 - 4 bytes
                raw_tcp_destination_port = tcp_header[4:8]
                tcp_destination_port = int(raw_tcp_destination_port,16)

                #sequence number 4 - 8 bytes
                raw_sequence_number = tcp_header[8:16]
                sequence_number = int(raw_sequence_number, 16)

                #acknowledgement number 8 - 12 bytes
                raw_acknowledgement_number = tcp_header[16:24]
                acknowledgement_number = int(raw_acknowledgement_number, 16)

                # #tcp header length 12' bytes
                # raw_tcp_header_length = tcp_header[24:26]
                # tcp_header_length = bin(int(raw_tcp_header_length[0])) + bin(int(raw_tcp_header_length[1]))

                if tcp_destination_port == 80:
                    raw_payload = bytes_array[132:]
                    readable_payload = bytearray.fromhex(raw_payload).decode()

                    obj_tcp_header = TCPHeader(tcp_source_port, tcp_destination_port, sequence_number,
                                               acknowledgement_number, readable_payload)
                else:
                    obj_tcp_header = TCPHeader(tcp_source_port, tcp_destination_port, sequence_number,
                                               acknowledgement_number)


        network_packet = NetworkPacket(obj_ieee_header, obj_ip_header, obj_tcp_header)
        self.packets.append(network_packet)
        self.gui.add_packet(network_packet)

    def create_mac_addres(self, raw_mac_address):
        mac_address = raw_mac_address[0:2] + ':' + raw_mac_address[2:4] + ':' + raw_mac_address[4:6] + ':' + raw_mac_address[6:8] + ':' + raw_mac_address[8:10] + ':' + raw_mac_address[10:12]
        return mac_address.upper()

    def create_ip_address(self, raw_ip_address):
        ip = str(self.hex_to_dec(raw_ip_address[0:2], 16)) + "." + str(self.hex_to_dec(raw_ip_address[2:4], 16)) + "." + str(self.hex_to_dec(raw_ip_address[4:6], 16)) + "." + str(self.hex_to_dec(raw_ip_address[6:8], 16))
        return ip

    def wifi_packet_handler(self, packet):
        if packet.haslayer(Dot11):
            if packet.type == 0 and packet.subtype == 8:
                #only needed for python >2.7
                # hex_packet = bytes_hex(packet)
                bytes_array = bytes_hex(packet)

                # first 18 bytes = Radio Tap Header
                radio_tap_header = bytes_array[:36]
                #next 24 bytes = Beacon Frame
                beacon_frame = bytes_array[36:84]
                #next bytes wireless management
                wireless_management = bytes_array[84:]
                #tagged parameters starting from 13th byte
                tagged_parameters = wireless_management[24:]

                raw_ssid_length = tagged_parameters[2:4]
                ssid_length = self.hex_to_dec(raw_ssid_length, 16)
                raw_ssid = tagged_parameters[4:(4+ssid_length*2)]

                ssid =bytearray.fromhex(raw_ssid).decode()
                raw_mac_address = beacon_frame[20:32]
                mac_address = self.create_mac_addres(raw_mac_address)

                raw_signal_strength = radio_tap_header[28:30]
                signal_strength = self.hex_to_dec(raw_signal_strength, 8)

                raw_channel = radio_tap_header[30:32]
                channel = self.hex_to_dec(raw_channel, 16)

                raw_frequency_bg = radio_tap_header[20:24]
                raw_frequency = raw_frequency_bg[2:] + raw_frequency_bg[:2]
                frequency = self.hex_to_dec(raw_frequency, 16)

                ssid = ssid.replace(' ', '')
                if len(ssid) > 0:
                    network_info = NetworkInfo(ssid, mac_address, signal_strength, channel, frequency)
                    if mac_address not in self.networks.keys():
                        self.gui.add_wifi_info(network_info)
                        self.networks[mac_address] = network_info


    def should_stop_program(self, x):
        return self.stop_sniffer

    def set_stop_program(self):
        self.stop_sniffer = True

    def start_sniffer(self):
        sniff(iface="wlp3s0", prn=self.process_packet, stop_filter=self.should_stop_program)

    def start_wifi_analyser(self):
        sniff(iface="wlan0", prn=self.wifi_packet_handler, stop_filter=self.should_stop_program)