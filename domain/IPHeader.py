class IPHeader:

    def __init__(self, source_ip, destination_ip, time_to_live, protocol):
        self.source_ip = source_ip
        self.destination_ip = destination_ip
        self.time_to_live = time_to_live
        self.protocol = protocol

    def __str__(self):
        return 'Source IP:  ' + self.source_ip + ' | ' + \
               'Destination IP: ' + self.destination_ip  + ' | '+ \
               'Time to live: ' + str(self.time_to_live) + ' | ' + \
               'Protocol: ' + self.protocol + ' | *'