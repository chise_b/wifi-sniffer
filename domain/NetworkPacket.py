class NetworkPacket:

    def __init__(self, ieee_header, ip_header=None, tcp_header=None):
        self.ieee_header = ieee_header
        self.ip_header = ip_header
        self.tcp_header = tcp_header
