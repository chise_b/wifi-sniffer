
class Manufacturers:

    def __init__(self):
        self.manufacturers = self.read_manufacturers()

    def get_manufacturers(self):
        return self.manufacturers

    def read_manufacturers(self):
        filename = '../helpers/manufacturers.txt'
        file = open(filename, 'r')
        lines = file.readlines()
        manufacturers = {}
        for line in lines:
            splitted = line.split("\t")
            manufacturers[splitted[0]] = splitted[1];

        return manufacturers
