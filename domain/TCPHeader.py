class TCPHeader:

    def __init__(self, source_port, destination_port, sequence_number, acknowledgment_number, payload=None):
        self.source_port = source_port
        self.destination_port = destination_port
        self.sequence_number = sequence_number
        self.acknowledgment_number = acknowledgment_number
        self.payload = payload

    def __str__(self):
        base_str =  'Source port: ' + str(self.source_port) + ' | ' + \
                    'Destination port: ' + str(self.destination_port) + ' | ' + \
                    'Sequence number: ' + str(self.sequence_number) + ' | ' + \
                    'Acknowledgment number: ' + str(self.acknowledgment_number) + ' | '

        if  self.payload != None and len(self.payload) > 0:
            base_str += 'Payload: ' + self.payload + ' | '

        return base_str

