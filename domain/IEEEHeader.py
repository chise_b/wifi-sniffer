class IEEEHeader:

    def __init__(self, source_mac, destination_mac, type):
        self.source_mac = source_mac
        self.destination_mac = destination_mac
        self.type = type

    def __str__(self):
        return 'IEEE 802.11 Source MAC: ' + self.source_mac + ' | ' + \
               'IEEE 802.11 Destination MAC: ' + self.destination_mac + ' | ' + \
               'IEEE 802.11 Type: ' + self.type + ' | *'