from domain.Manufacturers import  Manufacturers

class NetworkInfo:

    def __init__(self, ssid, mac_address, signal_strength, channel, frequency):
        self.ssid = ssid
        self.mac_address = mac_address
        self.signal_strength = signal_strength
        self.channel = channel
        self.frequency = frequency
        self.manufacturer = self.get_manufacturer(mac_address)


    def get_manufacturer(self, mac_address):
        manufacturers_helper = Manufacturers()
        manufacturers = manufacturers_helper.get_manufacturers()
        searchable_mac_address = mac_address[:8]

        if searchable_mac_address in manufacturers:
            return manufacturers[searchable_mac_address]

        return "UNKNOWN"

    def __str__(self):
        return  'SSID: ' + self.ssid + ' | ' + \
                'MAC: ' + self.mac_address + ' | ' + \
                'Strength: ' + str(self.signal_strength) + 'db | ' + \
                'Channel: ' + str(self.channel) + ' | ' + \
                'Frequency: ' + str(self.frequency) + ' | ' + \
                'Manufacturer: ' + self.manufacturer + ' | '




