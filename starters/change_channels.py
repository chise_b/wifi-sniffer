from threading import Thread
import subprocess,shlex,time
import threading
locky = threading.Lock()

def Change_Freq_channel(channel_c):
    print('Channel:',str(channel_c))
    command = 'iwconfig wlan0 channel '+ str(channel_c)
    command = shlex.split(command)
    subprocess.Popen(command,shell=False) # To prevent shell injection attacks !

if __name__ == '__main__':
    while True:
        for channel_c in range(1, 15):
            t = Thread(target=Change_Freq_channel, args=(channel_c,))
            t.daemon = True
            locky.acquire()
            t.start()
            time.sleep(0.5)
            locky.release()