from scapy.all import *
from helpers.Parser import Parser
from GUI.GUI import ProgramGUI
from threading import Timer


if __name__ == '__main__':
    gui = ProgramGUI()
    parser = Parser(gui)

    gui.add_pause_button(parser.set_stop_program)

    r = Timer(10.0, parser.start_sniffer())
    r.start()

    window = gui.get_window()
    window.mainloop()